package com.ppp.travelagency.service;

import static org.assertj.core.groups.Tuple.tuple;
import static org.mockito.Mockito.when;

import com.ppp.travelagency.config.ApplicationConfiguration;
import com.ppp.travelagency.model.City;
import com.ppp.travelagency.model.Trip;
import com.ppp.travelagency.repository.TripRepository;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(properties = {
    "ppp.app.configuration.zoneId=Europe/Warsaw",
    "ppp.app.configuration.topTripNumber=3",
    "ppp.app.configuration.defaultLastMinuteSearchPeriod=10",
    "ppp.app.configuration.defaultGlobalSearchPeriod=10",
    "ppp.app.configuration.smallestFreeReservationNumberForAdult=1",
    "ppp.app.configuration.smallestFreeReservationNumberForChildren=1",
    "ppp.app.configuration.discountLowerLimit=10",
    "spring.liquibase.enabled=false"
})
class TripServiceTest {

    private static final Instant LAST_DEPARTURE_DATE_TIME = Clock.fixed(Instant.parse("2020-04-25T10:00:00Z"), ZoneOffset.UTC).instant();

    @MockBean
    private TripRepository tripRepository;

    @Autowired
    private TripService tripService;

    @Autowired
    private ApplicationConfiguration applicationConfiguration;

    @Test
    void testFindTopTripsWithBestOffer() {
        //given
        when(tripRepository.findTrip(BigDecimal.valueOf(applicationConfiguration.getDiscountLowerLimit()),
            applicationConfiguration.getSmallestFreeReservationNumberForAdult(),
            applicationConfiguration.getSmallestFreeReservationNumberForChildren(),
            LAST_DEPARTURE_DATE_TIME.plus(applicationConfiguration.getDefaultGlobalSearchPeriod(), ChronoUnit.DAYS)))
            .thenReturn(List.of(
                prepareTrip(BigDecimal.valueOf(50L), Instant.parse("2020-04-24T10:00:00Z"), null),
                prepareTrip(BigDecimal.valueOf(40L), Instant.parse("2020-04-22T10:00:00Z"), null),
                prepareTrip(BigDecimal.valueOf(10L), Instant.parse("2020-04-20T10:00:00Z"), null),
                prepareTrip(BigDecimal.valueOf(50L), Instant.parse("2020-04-23T10:00:00Z"), null),
                prepareTrip(BigDecimal.valueOf(20L), Instant.parse("2020-04-21T10:00:00Z"), null)
            ));

        //when
        List<Trip> actualResult = tripService.getTopTripsWithBestOffer(LAST_DEPARTURE_DATE_TIME);
        //then
        Assertions.assertThat(actualResult)
            .extracting(
                Trip::getDiscount,
                Trip::getDepartureDateTime
            )
            .containsExactly(
                tuple(BigDecimal.valueOf(10L), Instant.parse("2020-04-20T10:00:00Z")),
                tuple(BigDecimal.valueOf(20L), Instant.parse("2020-04-21T10:00:00Z")),
                tuple(BigDecimal.valueOf(40L), Instant.parse("2020-04-22T10:00:00Z"))
            );
    }

    @Test
    void testGetTripWithBestOffer() {
        //given
        when(tripRepository.findTrip(BigDecimal.valueOf(applicationConfiguration.getDiscountLowerLimit()),
            applicationConfiguration.getSmallestFreeReservationNumberForAdult(),
            applicationConfiguration.getSmallestFreeReservationNumberForChildren(),
            LAST_DEPARTURE_DATE_TIME.plus(applicationConfiguration.getDefaultGlobalSearchPeriod(), ChronoUnit.DAYS)))
            .thenReturn(List.of(
                prepareTrip(BigDecimal.valueOf(50L), Instant.parse("2020-04-24T10:00:00Z"), null),
                prepareTrip(BigDecimal.valueOf(40L), Instant.parse("2020-04-22T10:00:00Z"), null),
                prepareTrip(BigDecimal.valueOf(10L), Instant.parse("2020-04-20T10:00:00Z"), null),
                prepareTrip(BigDecimal.valueOf(50L), Instant.parse("2020-04-23T10:00:00Z"), null),
                prepareTrip(BigDecimal.valueOf(20L), Instant.parse("2020-04-21T10:00:00Z"), null)
            ));

        //when
        Trip actualResult = tripService.getTripWithBestOffer(LAST_DEPARTURE_DATE_TIME);
        //then
        Assertions.assertThat(actualResult)
            .extracting(
                Trip::getDiscount,
                Trip::getDepartureDateTime
            )
            .containsExactly(BigDecimal.valueOf(50L), Instant.parse("2020-04-23T10:00:00Z"));
    }

    @Test
    void testGetLastMinuteTrips() {
        //given
        when(tripRepository.findTrip(applicationConfiguration.getSmallestFreeReservationNumberForAdult(),
            applicationConfiguration.getSmallestFreeReservationNumberForChildren(),
            LAST_DEPARTURE_DATE_TIME.plus(applicationConfiguration.getDefaultLastMinuteSearchPeriod(), ChronoUnit.DAYS)))
            .thenReturn(List.of(
                prepareTrip(BigDecimal.valueOf(50L), Instant.parse("2020-04-24T10:00:00Z"), null),
                prepareTrip(BigDecimal.valueOf(40L), Instant.parse("2020-04-22T10:00:00Z"), null),
                prepareTrip(BigDecimal.valueOf(10L), Instant.parse("2020-04-20T10:00:00Z"), null),
                prepareTrip(BigDecimal.valueOf(50L), Instant.parse("2020-04-23T10:00:00Z"), null),
                prepareTrip(BigDecimal.valueOf(20L), Instant.parse("2020-04-21T10:00:00Z"), null),
                prepareTrip(BigDecimal.valueOf(20L), Instant.parse("2020-04-24T10:00:00Z"), null)
            ));

        //when
        List<Trip> actualResult = tripService.getLastMinuteTrips(LAST_DEPARTURE_DATE_TIME);
        //then
        Assertions.assertThat(actualResult)
            .extracting(
                Trip::getDiscount,
                Trip::getDepartureDateTime
            )
            .containsExactly(
                tuple(BigDecimal.valueOf(10L), Instant.parse("2020-04-20T10:00:00Z")),
                tuple(BigDecimal.valueOf(20L), Instant.parse("2020-04-21T10:00:00Z")),
                tuple(BigDecimal.valueOf(40L), Instant.parse("2020-04-22T10:00:00Z")),
                tuple(BigDecimal.valueOf(50L), Instant.parse("2020-04-23T10:00:00Z")),
                tuple(BigDecimal.valueOf(50L), Instant.parse("2020-04-24T10:00:00Z")),
                tuple(BigDecimal.valueOf(20L), Instant.parse("2020-04-24T10:00:00Z"))
            );
    }

    @Test
    void testGetGlobalTrips() {
        //given
        when(tripRepository.findTrip(applicationConfiguration.getSmallestFreeReservationNumberForAdult(),
            applicationConfiguration.getSmallestFreeReservationNumberForChildren(),
            LAST_DEPARTURE_DATE_TIME.plus(applicationConfiguration.getDefaultLastMinuteSearchPeriod(), ChronoUnit.DAYS)))
            .thenReturn(List.of(
                prepareTrip(null, Instant.parse("2020-04-24T10:00:00Z"), prepareCity("New York")),
                prepareTrip(null, Instant.parse("2020-04-22T10:00:00Z"), prepareCity("New York")),
                prepareTrip(null, Instant.parse("2020-04-20T10:00:00Z"), prepareCity("New York")),
                prepareTrip(null, Instant.parse("2020-04-23T10:00:00Z"), prepareCity("New York")),
                prepareTrip(null, Instant.parse("2020-04-21T10:00:00Z"), prepareCity("New York")),
                prepareTrip(null, Instant.parse("2020-04-24T10:00:00Z"), prepareCity("Berlin"))
            ));

        //when
        List<Trip> actualResult = tripService.getGlobalTrips(LAST_DEPARTURE_DATE_TIME);
        //then
        Assertions.assertThat(actualResult)
            .extracting(
                Trip::getDepartureDateTime,
                trip -> trip.getTo().getName()
            )
            .containsExactly(
                tuple(Instant.parse("2020-04-20T10:00:00Z"), "New York"),
                tuple(Instant.parse("2020-04-21T10:00:00Z"), "New York"),
                tuple(Instant.parse("2020-04-22T10:00:00Z"), "New York"),
                tuple(Instant.parse("2020-04-23T10:00:00Z"), "New York"),
                tuple(Instant.parse("2020-04-24T10:00:00Z"), "Berlin"),
                tuple(Instant.parse("2020-04-24T10:00:00Z"), "New York")
            );
    }

    @Test
    void testGetTripWithOneResult() {
        //given
        when(tripRepository.findTripById(1L))
            .thenReturn(
                Optional.of(prepareTrip(BigDecimal.valueOf(10L), Instant.parse("2020-04-24T10:00:00Z"), prepareCity("New York")))
            );

        //when
        Trip actualResult = tripService.getTrip(1L);
        //then
        Assertions.assertThat(actualResult).extracting(
            Trip::getDiscount,
            Trip::getDepartureDateTime,
            trip -> trip.getTo().getName()
        ).contains(
            BigDecimal.valueOf(10L),
            Instant.parse("2020-04-24T10:00:00Z"),
            "New York"
        );
    }

    @Test
    void testGetTripWithException() {
        //given
        when(tripRepository.findTripById(1L))
            .thenReturn(Optional.empty());

        //when & then
        Assertions.assertThatExceptionOfType(RuntimeException.class)
            .isThrownBy(() -> tripService.getTrip(1L))
            .withMessage("The trip can't be found.");
    }

    private Trip prepareTrip(BigDecimal discount,
                             Instant departureDateTime,
                             City to) {
        return Trip.builder()
            .discount(discount)
            .departureDateTime(departureDateTime)
            .to(to)
            .build();
    }

    private City prepareCity(String name) {
        return City.builder()
            .name(name)
            .build();
    }

}
