package com.ppp.travelagency.dto.mapper;

import com.ppp.travelagency.dto.ImageDto;
import com.ppp.travelagency.model.Image;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ImageDtoMapper {

    public ImageDto convert(Image image) {
        return ImageDto.builder()
            .id(image.getId())
            .name(image.getName())
            .imageType(image.getImageType())
            .imageBytes(image.getImageBytes())
            .build();
    }

}
