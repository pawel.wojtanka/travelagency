package com.ppp.travelagency.dto.mapper;

import com.ppp.travelagency.dto.CountryDto;
import com.ppp.travelagency.model.Country;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CountryDtoMapper {

    private final ContinentDtoMapper continentDtoMapper;

    public CountryDto convert(Country country) {
        return CountryDto.builder()
            .id(country.getId())
            .name(country.getName())
            .continent(continentDtoMapper.convert(country.getContinent()))
            .build();
    }

}
