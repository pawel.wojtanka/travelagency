package com.ppp.travelagency.dto;

import com.ppp.travelagency.model.ContinentEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContinentDto {

    private Long id;
    private ContinentEnum name;

}
