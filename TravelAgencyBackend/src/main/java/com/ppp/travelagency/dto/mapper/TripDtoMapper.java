package com.ppp.travelagency.dto.mapper;

import com.ppp.travelagency.config.ApplicationConfiguration;
import com.ppp.travelagency.dto.TripDto;
import com.ppp.travelagency.model.Trip;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TripDtoMapper {

    private final CityDtoMapper cityDtoMapper;
    private final ApplicationConfiguration applicationConfiguration;
    private final ImageDtoMapper imageDtoMapper;

    public List<TripDto> getTripDtoList(List<Trip> trips) {
        return trips.stream()
            .map(this::convert)
            .collect(Collectors.toList());
    }

    public TripDto convert(Trip trip) {
        return TripDto.builder()
            .id(trip.getId())
            .name(trip.getName())
            .from(cityDtoMapper.convert(trip.getFrom()))
            .to(cityDtoMapper.convert(trip.getTo()))
            .departureDateTime(trip.getDepartureDateTime().atZone(applicationConfiguration.getZoneId()))
            .returnDateTime(trip.getReturnDateTime().atZone(applicationConfiguration.getZoneId()))
            .stayDays(trip.getStayDays())
            .adultPrice(trip.getAdultPrice())
            .childPrice(trip.getChildPrice())
            .discount(trip.getDiscount())
            .mode(trip.getMode())
            .totalFreeReservationNumberForAdults(trip.getTotalFreeReservationNumberForAdults())
            .totalFreeReservationNumberForChildren(trip.getTotalFreeReservationNumberForChildren())
            .image(imageDtoMapper.convert(trip.getImage()))
            .build();
    }

}
