package com.ppp.travelagency.dto;

import com.ppp.travelagency.model.TripMode;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TripDto {

    private Long id;
    private String name;
    private ZonedDateTime departureDateTime;
    private ZonedDateTime returnDateTime;
    private Long stayDays;
    private TripMode mode;
    private BigDecimal adultPrice;
    private BigDecimal childPrice;
    private BigDecimal discount;
    private Long totalFreeReservationNumberForAdults;
    private Long totalFreeReservationNumberForChildren;
    private CityDto from;
    private CityDto to;
    private ImageDto image;

}
