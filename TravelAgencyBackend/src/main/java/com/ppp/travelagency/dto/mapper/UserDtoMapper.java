package com.ppp.travelagency.dto.mapper;

import com.ppp.travelagency.dto.UserDto;
import com.ppp.travelagency.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserDtoMapper {

    public UserDto convert(User user) {
        return UserDto.builder()
            .email(user.getEmail())
            .password(user.getPassword())
            .build();
    }

}
