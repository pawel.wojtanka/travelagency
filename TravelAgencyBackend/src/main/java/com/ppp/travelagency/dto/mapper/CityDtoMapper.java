package com.ppp.travelagency.dto.mapper;

import com.ppp.travelagency.dto.CityDto;
import com.ppp.travelagency.model.City;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CityDtoMapper {

    private final CountryDtoMapper countryDtoMapper;

    public CityDto convert(City city) {
        return CityDto.builder()
            .id(city.getId())
            .name(city.getName())
            .country(countryDtoMapper.convert(city.getCountry()))
            .build();
    }

}
