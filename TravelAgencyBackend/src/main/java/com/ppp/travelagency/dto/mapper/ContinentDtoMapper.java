package com.ppp.travelagency.dto.mapper;

import com.ppp.travelagency.dto.ContinentDto;
import com.ppp.travelagency.model.Continent;
import org.springframework.stereotype.Component;

@Component
public class ContinentDtoMapper {

    public ContinentDto convert(Continent continent) {
        return ContinentDto.builder()
            .id(continent.getId())
            .name(continent.getContinent())
            .build();
    }

}
