package com.ppp.travelagency.service;

import com.ppp.travelagency.model.User;
import com.ppp.travelagency.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public User getUser(String email) {
        return userRepository.findUserByEmail(email)
            .orElseThrow(() -> new RuntimeException("User can't be found."));
    }

}
