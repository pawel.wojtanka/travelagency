package com.ppp.travelagency.service;

import com.ppp.travelagency.config.ApplicationConfiguration;
import com.ppp.travelagency.model.Trip;
import com.ppp.travelagency.repository.TripRepository;
import com.ppp.travelagency.utils.DepartureDateTimeAscDiscountDescTripComparator;
import com.ppp.travelagency.utils.DepartureDateTimeAscToCityAscTripComparator;
import com.ppp.travelagency.utils.DiscountDescDepartureDateTimeAscTripComparator;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TripService {

    private final TripRepository tripRepository;
    private final ApplicationConfiguration applicationConfiguration;

    public Trip getTrip(Long id) {
        return tripRepository.findTripById(id)
            .orElseThrow(() -> new RuntimeException("The trip can't be found."));
    }

    public List<Trip> getTopTripsWithBestOffer(Instant lastDepartureDateTime) {
        return tripRepository.findTrip(BigDecimal.valueOf(applicationConfiguration.getDiscountLowerLimit()),
            applicationConfiguration.getSmallestFreeReservationNumberForAdult(),
            applicationConfiguration.getSmallestFreeReservationNumberForChildren(),
            lastDepartureDateTime.plus(applicationConfiguration.getDefaultGlobalSearchPeriod(), ChronoUnit.DAYS)).stream()
            .sorted(Comparator.comparing(Trip::getDepartureDateTime))
            .limit(applicationConfiguration.getTopTripNumber())
            .collect(Collectors.toList());
    }

    public Trip getTripWithBestOffer(Instant lastDepartureDateTime) {
        return tripRepository.findTrip(BigDecimal.valueOf(applicationConfiguration.getDiscountLowerLimit()),
            applicationConfiguration.getSmallestFreeReservationNumberForAdult(),
            applicationConfiguration.getSmallestFreeReservationNumberForChildren(),
            lastDepartureDateTime.plus(applicationConfiguration.getDefaultGlobalSearchPeriod(), ChronoUnit.DAYS)).stream()
            .min(new DiscountDescDepartureDateTimeAscTripComparator())
            .orElseThrow(() -> new RuntimeException("The Trip object can't be found."));
    }

    public List<Trip> getLastMinuteTrips(Instant lastDepartureDateTime) {
        return tripRepository.findTrip(applicationConfiguration.getSmallestFreeReservationNumberForAdult(),
            applicationConfiguration.getSmallestFreeReservationNumberForChildren(),
            lastDepartureDateTime.plus(applicationConfiguration.getDefaultLastMinuteSearchPeriod(), ChronoUnit.DAYS)).stream()
            .sorted(new DepartureDateTimeAscDiscountDescTripComparator())
            .collect(Collectors.toList());
    }

    public List<Trip> getGlobalTrips(Instant lastDepartureDateTime) {
        return tripRepository.findTrip(applicationConfiguration.getSmallestFreeReservationNumberForAdult(),
            applicationConfiguration.getSmallestFreeReservationNumberForChildren(),
            lastDepartureDateTime.plus(applicationConfiguration.getDefaultGlobalSearchPeriod(), ChronoUnit.DAYS)).stream()
            .sorted(new DepartureDateTimeAscToCityAscTripComparator())
            .collect(Collectors.toList());
    }

    public void addTrip(Trip trip) {
        tripRepository.save(trip);
    }

}
