package com.ppp.travelagency.utils;

import com.ppp.travelagency.model.Trip;
import java.util.Comparator;

public class DepartureDateTimeAscDiscountDescTripComparator implements Comparator<Trip> {

    @Override
    public int compare(Trip trip1, Trip trip2) {
        int discountCompare = trip2.getDiscount().compareTo(trip1.getDiscount());
        int departureDateTimeCompare = trip1.getDepartureDateTime().compareTo(trip2.getDepartureDateTime());

        if (departureDateTimeCompare == 0) {
            return discountCompare == 0
                ? departureDateTimeCompare
                : discountCompare;
        } else {
            return departureDateTimeCompare;
        }
    }

}
