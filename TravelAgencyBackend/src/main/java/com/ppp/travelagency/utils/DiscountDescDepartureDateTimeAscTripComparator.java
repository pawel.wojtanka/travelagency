package com.ppp.travelagency.utils;

import com.ppp.travelagency.model.Trip;
import java.util.Comparator;

public class DiscountDescDepartureDateTimeAscTripComparator implements Comparator<Trip> {

    @Override
    public int compare(Trip trip1, Trip trip2) {
        int discountCompare = trip2.getDiscount().compareTo(trip1.getDiscount());
        int departureDateTimeCompare = trip1.getDepartureDateTime().compareTo(trip2.getDepartureDateTime());

        if (discountCompare == 0) {
            return departureDateTimeCompare == 0
                ? discountCompare
                : departureDateTimeCompare;
        } else {
            return discountCompare;
        }
    }

}
