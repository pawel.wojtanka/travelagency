package com.ppp.travelagency.utils;

import com.ppp.travelagency.model.Trip;
import java.util.Comparator;

public class DepartureDateTimeAscToCityAscTripComparator implements Comparator<Trip> {

    @Override
    public int compare(Trip trip1, Trip trip2) {
        int toCityCompare = trip1.getTo().getName().compareTo(trip2.getTo().getName());
        int departureDateTimeCompare = trip1.getDepartureDateTime().compareTo(trip2.getDepartureDateTime());

        if (departureDateTimeCompare == 0) {
            return toCityCompare == 0
                ? departureDateTimeCompare
                : toCityCompare;
        } else {
            return departureDateTimeCompare;
        }
    }

}
