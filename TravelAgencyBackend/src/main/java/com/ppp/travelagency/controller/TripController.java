package com.ppp.travelagency.controller;

import com.ppp.travelagency.config.ApplicationConfiguration;
import com.ppp.travelagency.dto.TripDto;
import com.ppp.travelagency.dto.mapper.TripDtoMapper;
import com.ppp.travelagency.model.Trip;
import com.ppp.travelagency.service.TripService;
import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
@RequestMapping(value = "/trips")
@RequiredArgsConstructor
public class TripController {

    private final TripService tripService;
    private final TripDtoMapper tripDtoMapper;
    private final ApplicationConfiguration applicationConfiguration;

    @GetMapping(value = "/best-offers")
    @ResponseBody
    public List<TripDto> getTopTripsWithBestOffer() {
        Instant nowPlusNumberOfDays = Instant.now(Clock.systemUTC()).plus(applicationConfiguration.getDefaultGlobalSearchPeriod(), ChronoUnit.DAYS);
        List<Trip> trips = tripService.getTopTripsWithBestOffer(nowPlusNumberOfDays);

        return tripDtoMapper.getTripDtoList(trips);
    }

    @GetMapping(value = "/best-offer")
    @ResponseBody
    public TripDto getTripWithBestOffer() {
        Instant nowPlusNumberOfDays = Instant.now(Clock.systemUTC()).plus(applicationConfiguration.getDefaultGlobalSearchPeriod(), ChronoUnit.DAYS);
        Trip trip = tripService.getTripWithBestOffer(nowPlusNumberOfDays);

        return tripDtoMapper.convert(trip);
    }

    @GetMapping(value = "/last-minute-offers")
    @ResponseBody
    public List<TripDto> getLastMinuteTrips() {
        Instant nowPlusNumberOfDays = Instant.now(Clock.systemUTC()).plus(applicationConfiguration.getDefaultLastMinuteSearchPeriod(), ChronoUnit.DAYS);
        List<Trip> trips = tripService.getLastMinuteTrips(nowPlusNumberOfDays);

        return tripDtoMapper.getTripDtoList(trips);
    }

    @GetMapping(value = "/global-offers")
    @ResponseBody
    public List<TripDto> getGlobalTrips() {
        Instant nowPlusNumberOfDays = Instant.now(Clock.systemUTC()).plus(applicationConfiguration.getDefaultGlobalSearchPeriod(), ChronoUnit.DAYS);
        List<Trip> trips = tripService.getGlobalTrips(nowPlusNumberOfDays);

        return tripDtoMapper.getTripDtoList(trips);
    }

    @GetMapping(value = "/details/{id}")
    @ResponseBody
    public TripDto getGlobalTrips(@PathVariable Long id) {
        Trip trip = tripService.getTrip(id);

        return tripDtoMapper.convert(trip);
    }

    @PostMapping(value = "/trip-adding/summary")
    public ResponseEntity<TripDto> createTrip(@Valid @RequestBody Trip body) {
        tripService.addTrip(body);

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
