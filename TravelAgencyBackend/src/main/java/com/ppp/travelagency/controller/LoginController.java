package com.ppp.travelagency.controller;

import com.ppp.travelagency.dto.UserDto;
import com.ppp.travelagency.model.User;
import com.ppp.travelagency.service.UserService;
import java.security.Principal;
import java.util.Base64;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequiredArgsConstructor
public class LoginController {

    private final UserService userService;

    @PostMapping("/login")
    public boolean login(@RequestBody UserDto userDto) {
        User user = userService.getUser(userDto.getEmail());

        return userDto.getPassword().equals(user.getPassword());
    }

    @RequestMapping(value = "/user")
    public Principal user(HttpServletRequest request) {
        String authToken = request.getHeader("Authorization")
            .substring("Basic".length()).trim();
        return () -> new String(Base64.getDecoder()
            .decode(authToken)).split(":")[0];
    }

}
