package com.ppp.travelagency.repository;

import com.ppp.travelagency.model.City;
import com.ppp.travelagency.model.Trip;
import com.ppp.travelagency.model.TripMode;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TripRepository extends JpaRepository<Trip, Long> {

    Optional<Trip> findTripById(Long id);

    @Query(value = "SELECT t FROM Trip t " +
        "WHERE t.totalFreeReservationNumberForAdults >= :smallestFreeReservationNumberForAdult " +
        "AND t.totalFreeReservationNumberForChildren >= :smallestFreeReservationNumberForChildren " +
        "AND t.departureDateTime <= :lastDepartureDateTime ")
    List<Trip> findTrip(@Param("smallestFreeReservationNumberForAdult") Long smallestFreeReservationNumberForAdult,
                        @Param("smallestFreeReservationNumberForChildren") Long smallestFreeReservationNumberForChildren,
                        @Param("lastDepartureDateTime") Instant lastDepartureDateTime);

    @Query(value = "SELECT t FROM Trip t " +
        "WHERE t.discount >= :smallestDiscount " +
        "AND t.totalFreeReservationNumberForAdults >= :smallestFreeReservationNumberForAdult " +
        "AND t.totalFreeReservationNumberForChildren >= :smallestFreeReservationNumberForChildren " +
        "AND t.departureDateTime <= :lastDepartureDateTime ")
    List<Trip> findTrip(@Param("smallestDiscount") BigDecimal smallestDiscount,
                        @Param("smallestFreeReservationNumberForAdult") Long smallestFreeReservationNumberForAdult,
                        @Param("smallestFreeReservationNumberForChildren") Long smallestFreeReservationNumberForChildren,
                        @Param("lastDepartureDateTime") Instant lastDepartureDateTime);

    @Query(value = "SELECT t FROM Trip t " +
        " LEFT JOIN FETCH t.from " +
        " LEFT JOIN FETCH t.to " +
        "WHERE t.discount >= :smallestDiscount " +
        "AND t.totalFreeReservationNumberForAdults >= :smallestFreeReservationNumberForAdult " +
        "AND t.totalFreeReservationNumberForChildren >= :smallestFreeReservationNumberForChildren " +
        "AND t.departureDateTime >= :startDateTime " +
        "AND t.returnDateTime <= :endDateTime " +
        "AND t.mode = :tripMode " +
        "AND t.from <= :from " +
        "AND t.to <= :to")
    List<Trip> findTripForPeriodAndFromCityToCity(@Param("smallestDiscount") BigDecimal smallestDiscount,
                                                  @Param("smallestFreeReservationNumberForAdult") Long smallestFreeReservationNumberForAdult,
                                                  @Param("smallestFreeReservationNumberForChildren") Long smallestFreeReservationNumberForChildren,
                                                  @Param("startDateTime") Instant startDateTime,
                                                  @Param("endDateTime") Instant endDateTime,
                                                  @Param("tripMode") TripMode tripMode,
                                                  @Param("from") City from,
                                                  @Param("to") City to);

}
