package com.ppp.travelagency.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = Airport.Table.NAME, schema = Constants.SCHEMA)
public class Airport {

    public interface Table {
        String NAME = "AIRPORTS";
    }

    public interface Columns {
        String ID = "ID";
        String NAME = "NAME";
        String CITY = "CITY_ID";
    }

    public interface Field {
        String CITY = "city";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Columns.ID, nullable = false)
    private Long id;

    @Column(name = Columns.NAME, nullable = false, length = 64)
    private String name;

    @ManyToOne
    @JoinColumn(name = Columns.CITY)
    private City city;

    @ManyToMany(mappedBy = Trip.Field.FROM_AIRPORTS)
    private List<Trip> trips;

}
