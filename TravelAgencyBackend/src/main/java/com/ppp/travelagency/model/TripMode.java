package com.ppp.travelagency.model;

public enum TripMode {

    BB,
    HB,
    FB,
    AI

}
