package com.ppp.travelagency.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = Role.Table.NAME)
public class Role {

    public interface Table {
        String NAME = "ROLES";
    }

    public interface Columns {
        String ID = "ID";
        String ROLE_NAME = "ROLE_NAME";
    }

    public interface Field {
        String USERS = "users";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Columns.ID, nullable = false)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(name = Columns.ROLE_NAME, nullable = false, length = 24)
    private RoleEnum role;

    @ManyToMany
    @JoinTable(name = "ROLES_USERS",
        schema = Constants.SCHEMA,
        joinColumns = @JoinColumn(name = "ROLE_ID"),
        inverseJoinColumns = @JoinColumn(name = "USER_ID"))
    private List<User> users;

}
