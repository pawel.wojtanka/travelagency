package com.ppp.travelagency.model;

public enum HotelStandard {

    ONE_STAR,
    TWO_STARS,
    THREE_STARS,
    FOUR_STARS,
    FIVE_STARS

}
