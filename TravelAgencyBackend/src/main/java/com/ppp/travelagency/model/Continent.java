package com.ppp.travelagency.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = Continent.Table.NAME, schema = Constants.SCHEMA)
public class Continent {

    public interface Table {
        String NAME = "CONTINENTS";
    }

    public interface Columns {
        String ID = "ID";
        String NAME = "NAME";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Columns.ID, nullable = false)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(name = Columns.NAME, nullable = false, length = 16)
    private ContinentEnum continent;

    @OneToMany(mappedBy = Country.Field.CONTINENT)
    private List<Country> counties;

}
