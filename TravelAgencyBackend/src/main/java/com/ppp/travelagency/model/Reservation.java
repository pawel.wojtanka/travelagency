package com.ppp.travelagency.model;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = Reservation.Table.NAME, schema = Constants.SCHEMA)
public class Reservation {

    public interface Table {
        String NAME = "RESERVATIONS";
    }

    public interface Columns {
        String ID = "ID";
        String TOTAL_PRICE = "TOTAL_PRICE";
        String RESERVATION_DATE_TIME = "RESERVATION_DATE_TIME";
        String TRIP = "TRIP_ID";
    }

    public interface Field {
        String TRIP = "trip";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Columns.ID, nullable = false)
    private Long id;

    @Column(name = Columns.TOTAL_PRICE, nullable = false)
    private BigDecimal totalPrice;

    @Column(name = Columns.RESERVATION_DATE_TIME, nullable = false)
    private Instant reservationDateTime;

    @ManyToOne
    @JoinColumn(name = Columns.TRIP, nullable = false)
    private Trip trip;

    @ManyToMany(mappedBy = User.Field.RESERVATIONS)
    private List<User> users;

}
