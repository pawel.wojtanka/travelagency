package com.ppp.travelagency.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = Hotel.Table.NAME, schema = Constants.SCHEMA)
public class Hotel {

    public interface Table {
        String NAME = "HOTELS";
    }

    public interface Columns {
        String ID = "ID";
        String NAME = "NAME";
        String HOTEL_STANDARD = "HOTEL_STANDARD";
        String CITY = "CITY_ID";
    }

    public interface Field {
        String CITY = "city";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Columns.ID, nullable = false)
    private Long id;

    @Column(name = Columns.NAME, nullable = false, length = 48)
    private String name;

    @Enumerated(value = EnumType.STRING)
    @Column(name = Columns.HOTEL_STANDARD, length = 16)
    private HotelStandard hotelStandard;

    @ManyToOne
    @JoinColumn(name = Columns.CITY)
    private City city;

}
