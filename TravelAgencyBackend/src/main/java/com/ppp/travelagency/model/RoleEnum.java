package com.ppp.travelagency.model;

public enum RoleEnum {

    ADMIN,
    EMPLOYEE,
    USER

}
