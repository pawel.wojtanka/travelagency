package com.ppp.travelagency.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = Country.Table.NAME, schema = Constants.SCHEMA)
public class Country {

    public interface Table {
        String NAME = "COUNTRIES";
    }

    public interface Columns {
        String ID = "ID";
        String NAME = "NAME";
        String CONTINENT = "CONTINENT_ID";
    }

    public interface Field {
        String CONTINENT = "continent";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Columns.ID, nullable = false)
    private Long id;

    @Column(name = Columns.NAME, nullable = false, length = 32)
    private String name;

    @ManyToOne
    @JoinColumn(name = Columns.CONTINENT)
    private Continent continent;

    @OneToMany(mappedBy = City.Field.COUNTRY)
    private List<City> cities;

}
