package com.ppp.travelagency.model;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = Trip.Table.NAME, schema = Constants.SCHEMA)
public class Trip {

    public interface Table {
        String NAME = "TRIPS";
    }

    public interface Columns {
        String ID = "ID";
        String NAME = "NAME";
        String DEPARTURE_DATE_TIME = "DEPARTURE_DATE_TIME";
        String RETURN_DATE_TIME = "RETURN_DATE_TIME";
        String STAY_DAYS = "STAY_DAYS";
        String MODE = "MODE";
        String ADULT_PRICE = "ADULT_PRICE";
        String CHILD_PRICE = "CHILD_PRICE";
        String DISCOUNT = "DISCOUNT";
        String TOTAL_RESERVATION_NUMBER_FOR_ADULTS = "TOTAL_RESERVATION_NUMBER_FOR_ADULTS";
        String TOTAL_RESERVATION_NUMBER_FOR_CHILDREN = "TOTAL_RESERVATION_NUMBER_FOR_CHILDREN";
        String TOTAL_FREE_RESERVATION_NUMBER_FOR_ADULTS = "TOTAL_FREE_RESERVATION_NUMBER_FOR_ADULTS";
        String TOTAL_FREE_RESERVATION_NUMBER_FOR_CHILDREN = "TOTAL_FREE_RESERVATION_NUMBER_FOR_CHILDREN";
        String FROM = "FROM_ID";
        String TO = "TO_ID";
        String TO_AIRPORT = "TO_AIRPORT_ID";
        String HOTEL = "HOTEL_ID";
        String IMAGE = "IMAGE_ID";
    }

    public interface Field {
        String FROM_AIRPORTS = "fromAirports";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Columns.ID, nullable = false)
    private Long id;

    @Column(name = Columns.NAME)
    private String name;

    @Column(name = Columns.DEPARTURE_DATE_TIME, nullable = false)
    private Instant departureDateTime;

    @Column(name = Columns.RETURN_DATE_TIME, nullable = false)
    private Instant returnDateTime;

    @Column(name = Columns.STAY_DAYS, nullable = false)
    private Long stayDays;

    @Enumerated(value = EnumType.STRING)
    @Column(name = Columns.MODE, length = 2)
    private TripMode mode;

    @Column(name = Columns.ADULT_PRICE, nullable = false)
    private BigDecimal adultPrice;

    @Column(name = Columns.CHILD_PRICE, nullable = false)
    private BigDecimal childPrice;

    @Column(name = Columns.DISCOUNT)
    private BigDecimal discount;

    @Column(name = Columns.TOTAL_RESERVATION_NUMBER_FOR_ADULTS, nullable = false)
    private Long totalReservationNumberForAdults;

    @Column(name = Columns.TOTAL_RESERVATION_NUMBER_FOR_CHILDREN, nullable = false)
    private Long totalReservationNumberForChildren;

    @Column(name = Columns.TOTAL_FREE_RESERVATION_NUMBER_FOR_ADULTS, nullable = false)
    private Long totalFreeReservationNumberForAdults;

    @Column(name = Columns.TOTAL_FREE_RESERVATION_NUMBER_FOR_CHILDREN, nullable = false)
    private Long totalFreeReservationNumberForChildren;

    @OneToOne
    @JoinColumn(name = Columns.FROM)
    private City from;

    @OneToOne
    @JoinColumn(name = Columns.TO)
    private City to;

    @ManyToMany
    @JoinTable(name = "TRIPS_FROM_AIRPORTS",
        schema = Constants.SCHEMA,
        joinColumns = @JoinColumn(name = "TRIP_ID"),
        inverseJoinColumns = @JoinColumn(name = "FROM_AIRPORT_ID"))
    private List<Airport> fromAirports;

    @OneToOne
    @JoinColumn(name = Columns.TO_AIRPORT)
    private Airport toAirport;

    @OneToOne
    @JoinColumn(name = Columns.HOTEL)
    private Hotel hotel;

    @OneToMany(mappedBy = Reservation.Field.TRIP)
    private List<Reservation> reservations;

    @OneToOne
    @JoinColumn(name = Columns.IMAGE)
    private Image image;

}
