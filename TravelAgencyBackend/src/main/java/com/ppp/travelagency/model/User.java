package com.ppp.travelagency.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = User.Table.NAME, schema = Constants.SCHEMA)
public class User {

    public interface Table {
        String NAME = "USERS";
    }

    public interface Columns {
        String ID = "ID";
        String FIRST_NAME = "FIRST_NAME";
        String LAST_NAME = "LAST_NAME";
        String PHONE_NUMBER = "PHONE_NUMBER";
        String EMAIL = "EMAIL";
        String PASSWORD = "PASSWORD";
    }

    public interface Field {
        String RESERVATIONS = "reservations";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Columns.ID, nullable = false)
    private Long id;

    @Column(name = Columns.FIRST_NAME, nullable = false, length = 32)
    private String firstName;

    @Column(name = Columns.LAST_NAME, nullable = false, length = 64)
    private String lastName;

    @Column(name = Columns.PHONE_NUMBER, length = 16)
    private String phoneNumber;

    @Column(name = Columns.EMAIL, length = 64)
    private String email;

    @Column(name = Columns.PASSWORD)
    private String password;

    @ManyToMany(mappedBy = Role.Field.USERS, fetch = FetchType.EAGER)
    private List<Role> roles;

    @ManyToMany
    @JoinTable(name = "USERS_RESERVATIONS",
        schema = Constants.SCHEMA,
        joinColumns = @JoinColumn(name = "USER_ID"),
        inverseJoinColumns = @JoinColumn(name = "RESERVATION_ID"))
    private List<Reservation> reservations;

}
