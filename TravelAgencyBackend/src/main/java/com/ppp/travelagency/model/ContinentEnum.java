package com.ppp.travelagency.model;

public enum ContinentEnum {

    ASIA,
    EUROPE,
    AFRICA,
    NORTH_AMERICA,
    SOUTH_AMERICA,
    AUSTRALIA

}
