package com.ppp.travelagency.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = Image.Table.NAME, schema = Constants.SCHEMA)
public class Image {

    public interface Table {
        String NAME = "IMAGES";
    }

    public interface Columns {
        String ID = "ID";
        String NAME = "NAME";
        String IMAGE_TYPE = "IMAGE_TYPE";
        String IMAGE_BYTE = "IMAGE_BYTE";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Image.Columns.ID, nullable = false)
    private Long id;

    @Column(name = Columns.NAME, nullable = false, length = 64)
    private String name;

    @Column(name = Columns.IMAGE_TYPE, nullable = false)
    private String imageType;

    @Lob
    @Column(name = Image.Columns.IMAGE_BYTE, nullable = false, length = 1000)
    private byte[] imageBytes;

}
