package com.ppp.travelagency.config;

import java.time.ZoneId;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "ppp.app.configuration")
public class ApplicationConfiguration {

    private Long discountLowerLimit;
    private Long topTripNumber;
    private Long defaultLastMinuteSearchPeriod;
    private Long defaultGlobalSearchPeriod;
    private Long smallestFreeReservationNumberForAdult;
    private Long smallestFreeReservationNumberForChildren;
    private ZoneId zoneId;

}
