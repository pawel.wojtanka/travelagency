import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TripComponent} from './trip-component/trip.component';
import {MainComponent} from './main/main.component';
import {LoginComponent} from './login/login.component';
import {RegistrationComponent} from './registration/registration.component';
import {SearchComponent} from './search/search.component';
import {ContactComponent} from './contact/contact.component';
import {ClientPanelComponent} from './client-panel/client-panel.component';
import {AdminPanelComponent} from './admin-panel/admin-panel.component';
import {AddTripComponent} from './add-trip/add-trip.component';
import {TripDetailsComponent} from './trip-details/trip-details.component';

@NgModule({
  declarations: [
    AppComponent,
    TripComponent,
    MainComponent,
    LoginComponent,
    RegistrationComponent,
    SearchComponent,
    ContactComponent,
    ClientPanelComponent,
    AdminPanelComponent,
    AddTripComponent,
    TripDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
