import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Trip} from "../model/trip";
import {City} from "../model/city";

@Component({
  selector: 'app-add-trip',
  templateUrl: './add-trip.component.html',
  styleUrls: ['./add-trip.component.css']
})
export class AddTripComponent implements OnInit {
  @Output() addTrip = new EventEmitter<Trip>();
  trip: Trip;

  constructor() {
  }

  ngOnInit(): void {
    this.trip = new Trip();
  }

  createTrip(name: string, departureDateTime: string, returnDateTime: string, stayDays: number, mode: string, adultPrice: number, childPrice: number, discount: number, totalFreeReservationNumberForAdults: number, totalFreeReservationNumberForChildren: number, from: City, to: City) {
    const trip = new Trip();
    trip.name = name;
    trip.departureDateTime = departureDateTime;
    trip.returnDateTime = returnDateTime;
    trip.stayDays = stayDays;
    trip.mode = mode;
    trip.adultPrice = adultPrice;
    trip.childPrice = childPrice;
    trip.discount = discount;
    trip.totalFreeReservationNumberForAdults = totalFreeReservationNumberForAdults;
    trip.totalFreeReservationNumberForChildren = totalFreeReservationNumberForChildren;
    trip.from = from;
    trip.to = to;
  }
}
