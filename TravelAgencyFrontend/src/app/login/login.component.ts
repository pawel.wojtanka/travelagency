import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {User} from "../model/user";
import {UserService} from "../service/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User = new User("", "");

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private userService: UserService
  ) {
  }

  ngOnInit() {
    sessionStorage.setItem('token', '');
  }

  login(): void {
    this.userService.login(this.user.email, this.user.password).subscribe(
      isValid => {
        if (isValid) {
          sessionStorage.setItem(
            'token',
            btoa(this.user.email + ':' + this.user.password)
          );
          this.router.navigate(['main']);
        } else {
          alert("Authentication failed.")
        }
      }
    );
  }

}
