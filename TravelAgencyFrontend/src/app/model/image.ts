export class Image {
  id: number;
  name: string;
  imageBytes: string;
}
