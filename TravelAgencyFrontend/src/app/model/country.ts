import {Continent} from './continent';

export class Country {
  id: number;
  name: string;
  continent: Continent;
}
