import {City} from './city';
import {Image} from "./image";

export class Trip {
  id: number;
  name: string;
  departureDateTime: string;
  returnDateTime: string;
  stayDays: number;
  mode: string;
  adultPrice: number;
  childPrice: number;
  discount: number;
  totalFreeReservationNumberForAdults: number;
  totalFreeReservationNumberForChildren: number;
  from: City;
  to: City;
  image: Image;
}
