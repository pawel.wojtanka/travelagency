import {Component, OnInit} from '@angular/core';
import {Trip} from '../model/trip';
import {TripService} from '../service/trip.service';

@Component({
  selector: 'app-trip-component',
  templateUrl: './trip.component.html',
  styleUrls: ['./trip.component.css']
})
export class TripComponent implements OnInit {

  bestOffer: Trip;
  bestOffers: Array<Trip>;
  lastMinuteOffers: Array<Trip>;
  globalOffers: Array<Trip>;

  constructor(private tripService: TripService) {
  }

  ngOnInit(): void {
    this.tripService.getBestOffer().subscribe(
      response => {
        this.bestOffer = response;
      });
    this.tripService.getBestOffers().subscribe(
      response => {
        this.bestOffers = response;
      });
    this.tripService.getLastMinuteOffers().subscribe(
      response => {
        this.lastMinuteOffers = response;
      });
    this.tripService.getGlobalOffers().subscribe(
      response => {
        this.globalOffers = response;
      });
  }

}
