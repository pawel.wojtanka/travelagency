import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {TripComponent} from './trip-component/trip.component';
import {MainComponent} from "./main/main.component";
import {LoginComponent} from "./login/login.component";
import {RegistrationComponent} from "./registration/registration.component";
import {ClientPanelComponent} from "./client-panel/client-panel.component";
import {AdminPanelComponent} from "./admin-panel/admin-panel.component";
import {ContactComponent} from "./contact/contact.component";
import {SearchComponent} from "./search/search.component";
import {AddTripComponent} from "./add-trip/add-trip.component";
import {TripDetailsComponent} from "./trip-details/trip-details.component";

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'main', component: MainComponent},
  {path: 'trips', component: TripComponent},
  {path: 'search', component: SearchComponent},
  {path: 'login', component: LoginComponent},
  {path: 'registration', component: RegistrationComponent},
  {path: 'clientpanel', component: ClientPanelComponent},
  {path: 'adminpanel', component: AdminPanelComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'add-trip', component: AddTripComponent},
  {path: 'trip-details/:id', component: TripDetailsComponent},
  {path: '**', redirectTo: '/main', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
