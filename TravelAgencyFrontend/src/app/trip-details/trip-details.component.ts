import {Component, OnInit} from '@angular/core';
import {Trip} from "../model/trip";
import {TripService} from "../service/trip.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-trip-details',
  templateUrl: './trip-details.component.html',
  styleUrls: ['./trip-details.component.css']
})
export class TripDetailsComponent implements OnInit {

  tripDetails: Trip;

  constructor(private activatedRoute: ActivatedRoute, private tripService: TripService) {
  }

  ngOnInit(): void {
    const id: number = parseInt(this.activatedRoute.snapshot.paramMap.get('id'), 10);
    this.tripService.getTripDetails(id).subscribe(
      response => {
        this.tripDetails = response;
      });
  }

}
