import {Component, OnInit} from '@angular/core';
import {TripService} from "../service/trip.service";
import {Trip} from "../model/trip";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  bestOffer: Trip;
  bestOffers: Array<Trip>;
  lastMinuteOffers: Array<Trip>;
  globalOffers: Array<Trip>;

  constructor(private tripService: TripService) {
  }

  ngOnInit(): void {
    this.tripService.getBestOffer().subscribe(
      response => {
        this.bestOffer = response;
      });
    this.tripService.getBestOffers().subscribe(
      response => {
        this.bestOffers = response;
      });
    this.tripService.getLastMinuteOffers().subscribe(
      response => {
        this.lastMinuteOffers = response;
      });
    this.tripService.getGlobalOffers().subscribe(
      response => {
        this.globalOffers = response;
      });
  }

}
