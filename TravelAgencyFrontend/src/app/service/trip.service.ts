import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Trip} from '../model/trip';

@Injectable({
  providedIn: 'root'
})
export class TripService {

  private restUrl: string = 'http://localhost:8080/trips';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) {
  }

  getBestOffer(): Observable<Trip> {
    const url: string = `${this.restUrl}/best-offer`;
    return this.httpClient.get<Trip>(url, this.httpOptions);
  }

  getBestOffers(): Observable<Trip[]> {
    const url: string = `${this.restUrl}/best-offers`;
    return this.httpClient.get<Trip[]>(url, this.httpOptions);
  }

  getLastMinuteOffers(): Observable<Trip[]> {
    const url: string = `${this.restUrl}/last-minute-offers`;
    return this.httpClient.get<Trip[]>(url, this.httpOptions);
  }

  getGlobalOffers(): Observable<Trip[]> {
    const url: string = `${this.restUrl}/global-offers`;
    return this.httpClient.get<Trip[]>(url, this.httpOptions);
  }

  getTripDetails(id: number): Observable<Trip> {
    const url: string = `${this.restUrl}/details/${id}`;
    return this.httpClient.get<Trip>(url, this.httpOptions);
  }

}
