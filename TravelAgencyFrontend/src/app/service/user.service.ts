import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../model/user";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private restUrl: string = 'http://localhost:8080/login';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) {
  }

  login(email: string, password: string): Observable<User> {
    return this.httpClient.post<User>(this.restUrl, UserService.createUser(email, password), this.httpOptions);
  }

  private static createUser(email: string, password: string): User {
    return new User(email, password);
  }

}
